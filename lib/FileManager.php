<?php
/**
 * Class to operate the file system
 * 
 * Unix only
 * 
 * @todo Add windows system support
 * @author V team
 */
 
namespace VLibrary\Core;

use \Exception;

class FileManager 
{
	
	public $basePath = './'; 
	
	public $FileSystem = FALSE;
	
	public $files = array();
	
	public $dirs = 	array();
	
	public $filesInfo = array();
	
	public $path = '';
	
	public function __construct(FileSystem $FileSystem)
	{
		$this -> FileSystem = $FileSystem;
	}

	
	/**
	 * scan directory and return array 
	 * ('files' => $files, 'dirs' => $dirs)
	 * 
	 * @todo recursive
	 * 
	 * @access 		public
	 * @param 		string 		directory path
	 * @return 		array 		see header 	
	 * */
	public function Dir()
	{
		$scanArray = $this -> FileSystem -> scanDir($this -> basePath.$this->getPath());
		
		$this -> files = $scanArray['files'];
		$this -> dirs = $scanArray['dirs'];
		$this -> filesInfo = $scanArray['filesinfo'];
	}
	
	
	/**
	 * Check is the file is a file relative to the base path
	 * return true if this is file is a not directory or empty 
	 * 
	 * @access public 
	 * @param 	string	file path
	 * @return 	bool	true or false
	 * */
	public function isFile($filePath)
	{
		if (!is_dir($this -> basePath.$filePath))
		{
			if (file_exists($this -> basePath.$filePath))
			{
				return TRUE;
			}
		}
		return FALSE;
	}
	

	/**
	 * Check is the dir is a dir relative to the base path
	 * return true if this is dir is a not file or empty 
	 * 
	 * @access public 
	 * @param 	string	file path
	 * @return 	bool	true or false
	 * */
	public function isDir($dirPath)
	{
		if (is_dir($this -> basePath.$filePath))
		{
			if (!file_exists($this -> basePath.$filePath))
			{
				return TRUE;
			}
		}
		return FALSE;
	}
	
	/**  
	 * Clear special 'giveup' chars and other spec chars from file path 
	 * return path without start slashes
	 * example input string .././///////...\.#$%^&*(*&^%$%^&*())\\\.....//..///..//./Vlibrary/../Database../
	 * return  this string Vlibrary/Database/
	 * 
	 * support english chars only
	 * 
	 * @todo support russian and other language
	 * 
	 * @access 	public
	 * @param 	string file path
	 * @return 	string clean file path
	 *  */
	public function normalizePath($filepath)
	{
		$filepath = trim($filepath);
	    $filepath = preg_replace('/([^a-zA-Z0-9])+([^a-zA-Z\_0-9\.\/-])/','', $filepath);
	    $filepath = preg_replace('/(?:\.|\/){1,}\//','/' , $filepath);
		$filepath = preg_replace('/^(\/){1,}|(\/){1,}$/', '',$filepath);
		if (strlen($filepath) > 4000)
			return FALSE;

		return $filepath;
	}
	
	
	/**
	 * set base path a root directory for dir and paths
	 * 
	 * @access 	public
	 * @param	string		base path end of '/' 
	 * @return 	null
	 * */
	public function setBasePath($basePath)
	{
		$this -> basePath = $basePath;
	}
	
	
	/**
	 * protected setter path string to $this -> path
	 * @see 	this -> normalizePath
	 * @access 	public
	 * @param	string		external path
	 * @return 	
	 * 
	 * */
	public function setPath($path)
	{
		$path = $this -> normalizePath($path);
		$this -> path = $path;
	}
	
	
	/**
	 * protected getter path string from $this -> path
	 * @see 	this -> setPath 
	 * @access 	public 
	 * @return	string		path 
	 * */
	public function getPath()
	{
		return $this -> path;
	}
	
	/**
	 * return a on up path folder
	 * @see 	this -> setPath 
	 * @access 	public 
	 * @return	string		path 
	 * */
	public function getUpPath()
	{
		 $dirname = dirname($this -> path);
		 if ($dirname =='.')
		 	return '';
		
		return $dirname;
	}
	
	
	
	/**
	 * Sort this array files
	 * 
	 * @access 	public
	 * @param	string 	sort type ASC or desc
	 * @return 	null 
	 * */
	public function sortFiles($sortType = 'asc')
	{
		$sortType = strtolower($sortType);
		if (empty($this -> files))
			return FALSE;

		if ($sortType == 'asc')
			sort($this -> files);
		else if ($sortType == 'desc')
			rsort($this -> files);
		else
			throw new Exception("This sort type not found ($sortType)", 1);

		return NULL;
	}


	/**
	 * Sort this array dirs
	 * 
	 * @access 	public
	 * @param	string 	sort type ASC or desc
	 * @return 	null 
	 * */
	public function sortDirs($sortType = 'asc')
	{
		$sortType = strtolower($sortType);
		if (empty($this -> dirs))
			return FALSE;

		if ($sortType == 'asc')
			sort($this -> dirs);
		else if ($sortType == 'desc')
			rsort($this -> dirs);
		else
			throw new Exception("This sort type not found ($sortType)", 1);

		return NULL;
	}
}

