<?php
/**
 * Class to operate the file system
 * 
 * Unix only
 * 
 * @todo Add windows system support
 * @author V team
 * @category Components
 * @package vbilling.core.components
 * @subpackage core
 * @version 0.0.2
 */
 
namespace VLibrary\Core;

use \Exception;

class FileSystem 
{
	
	/**
	 * @var переменная будет содержать в массиве другие переменные необходимые для передачи 
	 * */
	private $_localStorage = array();

	/**
	 * scan directory and return array 
	 * ('files' => $files, 'dirs' => $dirs)
	 * 
	 * @todo recursive
	 * 
	 * @access 		public
	 * @param 		string 		directory path
	 * @return 		array 		see header 	
	 * */
	public function scanDir($dirPath)
	{
		$path = realpath($dirPath);
		$path = $path;
		$files = array();
		$filesInfo = array();
		$directory = array();
			
		if ($fp = @opendir($path))
		{
			while (FALSE !== ($file = readdir($fp)))
			{
				
				if (is_dir($path.DIRECTORY_SEPARATOR.$file) && strncmp($file, '.', 1) !== 0)
				{
					$directory[] = $file;
				}
				elseif (strncmp($file, '.', 1) !== 0)
				{
					$files[] = $file;
					$filesInfo[$file] = $this -> getFileInfo($path.DIRECTORY_SEPARATOR.$file);
				}
			}
			return array('files' => $files,'dirs' => $directory,'filesinfo' => $filesInfo);
		}
		else
		{
			return FALSE;
		}
	
	}

	/**
	 * give a file info 
	 * 
	 * @access public 
	 * @param 	string		path to file
	 * @param	array 		return file param array
	 * @return	array 		file info array
	 * */	
	public function getFileInfo($filePath, $returned_values = array('name', 'server_path', 'size', 'date'))
	{

		if ( ! file_exists($filePath))
			throw new Exception("file '$filePath' not found ");
		if (is_string($returned_values))
			$returned_values = explode(',', $returned_values);
		foreach ($returned_values as $key)
		{
			switch ($key)
			{
				case 'name':
					$fileinfo['name'] = substr(strrchr($filePath, DIRECTORY_SEPARATOR), 1);
					break;
				case 'server_path':
					$fileinfo['server_path'] = $filePath;
					break;
				case 'size':
					$fileinfo['size'] = filesize($filePath);
					break;
				case 'date':
					$fileinfo['date'] = filemtime($filePath);
					break;
				case 'readable':
					$fileinfo['readable'] = is_readable($filePath);
					break;
				case 'writable':
					// There are known problems using is_weritable on IIS.  It may not be reliable - consider fileperms()
					$fileinfo['writable'] = is_writable($filePath);
					break;
				case 'executable':
					$fileinfo['executable'] = is_executable($filePath);
					break;
				case 'fileperms':
					$fileinfo['fileperms'] = fileperms($filePath);
					break;
			}
		}
		return $fileinfo;
	}
	
	/**
	 * Convert byte to round byte system
	 * 
	 * @access public
	 * @param 	string 	bytes
	 * @param   int		precision
	 * @return  string		true or false
	 * */
	function roundFileSize($bytes, $precision = 2) {
	    $units = array('B', 'KB', 'MB', 'GB', 'TB');
	    $bytes = max($bytes, 0);
	    $pow = floor(($bytes?log($bytes):0)/log(1024));
	    $pow = min($pow, count($units)-1);
	    $bytes /= pow(1024, $pow);
	    return round($bytes, $precision).' '.$units[$pow];
	}

	/**
	 * Return extension of a file name
	 * 
	 * @access public
	 * @param 	string 	file name
	 * @return  bool 	true or false
	 * */
	public function getFileExtension($fileName)
	{
		$_filename_array = explode('.', $filename);
		if (count($_filename_array)>1)
			return end($_filename_array);
		else
			return '';
	}
	
	
	/**
	 * Checks Correctly file name
	 * 
	 * @access public
	 * @param 	string 	file name
	 * @return  bool 	true or false
	 * */
	public function chackFileName($fileName)
	{
		if (preg_match("/(^[a-zA-Z0-9]+([a-zA-Z\_0-9\.-]*))$/" , $fileName) AND strlen($fileName) < 255)
			return TRUE;
		else
			return FALSE;
	}
	
}

