<?php
	include ("./lib/FileSystem.php");
	include ("./lib/FileManager.php");
	
	$config = include ("./config.php");

        if (!isset($_GET['path'])){
		$path = '';
        }else{
		$path = $_GET['path'];
        }
        
	$fileManager = new VLibrary\Core\FileManager(
		new VLibrary\Core\FileSystem()
	);

	$fileManager -> setBasePath($config['basePath']);
	$fileManager -> setPath($path);
	$backPath = $fileManager -> getUpPath();

        if (!$fileManager -> isFile($fileManager ->getPath()))
	{
		// scan dir
		$fileManager -> dir();
		// sort files
		$fileManager -> sortFiles();
		// sort dirs
		$fileManager -> sortDirs();
	}
	
        include ('views/start_content.php');
	include ('views/navbar.php');
	include ('views/breadcrumb.php');

	include ('views/content.php');
	include ('views/end_content.php');
	
?>