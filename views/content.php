<?php
	
	echo "<table width = '100%' class='table table-bordered table-striped' >";
	
	echo "<tr><td><i class='icon-arrow-up'></td><td><a href='?path=$backPath'>to up</a></td><td></td><td></td></tr>";
	if (!empty($fileManager->dirs))
	{
		foreach ($fileManager -> dirs as $key => $value)
		{
			$dirPath = $fileManager -> getPath().($fileManager -> getPath()?DIRECTORY_SEPARATOR:'').$value;
			
			echo "<tr>
			<td width ='1'><i class='icon-folder-close'></i></td>
			<td>
			
			<a href='?path=$dirPath'>";
				echo $value;
			echo "</a></td>
				<td>Folder</td><td></td>
			</tr>";
		}
	}
	
	if (!empty($fileManager->files))
	{
		foreach ($fileManager -> files as $key => $value)
		{
			echo "<tr>";
				echo "<td width='1'><i class='icon-file'></i>";
				echo "</td>";
				echo "<td>";
				
					echo $value;
				echo "</td>";
				echo "<td>";
					echo $fileManager -> FileSystem -> roundFileSize($fileManager -> filesInfo[$value]['size']);
				echo "</td>";
				echo "<td>";
					echo date('Y.m.d', $fileManager -> filesInfo[$value]['date']);
				echo "</td>";
			echo "</tr>";
		}
	}	
	echo "</table>";
?>